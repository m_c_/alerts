import logging
from telegram.ext import Updater
from notifiers.abstract import AbstractNotifier
from utils import format_config


class Notifier(AbstractNotifier):
    ''' '''

    _config_required = ('key', 'group')
    _config_allowed = ('title', 'key', 'group', 'proxy', 'debug')
    _config_defaults = {'debug': False}

    def notify(self, config, message):
        '''
        Main notifiyng function
        @param config: dict
        @param message: string
        '''
        cfg = format_config(config, self._config_allowed, self._config_required, self._config_defaults)
        if cfg['debug']:
            logging.debug('Sending message: {}'.format(message))
        else:
            args = {'proxy_url': cfg['proxy']} if cfg['proxy'] else None
            updater = Updater(cfg['key'], request_kwargs=args)
            updater.bot.sendMessage(cfg['group'], message)
