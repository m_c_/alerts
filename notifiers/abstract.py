class AbstractNotifier:
    ''' '''

    _config_required = ('url',)
    _config_allowed = ('title', 'user', 'pass', 'url', 'proxy')
    _config_defaults = {}

    def notify(self, config, message):
        '''
        Main notifiyng function
        @param config: dict
        @param message: string
        '''
        raise NotImplemented()
