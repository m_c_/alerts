CONFIG = {
    'console': False,
    'debug': False,
    'log': './alerts.log',
    'check': {
        'rabbitmq': [],
        'http': []
    },
    'notify': {
        'telegram': []
    }
}
'''
config for rabbitmq 
{
    'title': 'title',
    'user': 'user',
    'pass': 'pass',
    'host': '127.0.0.1',
    'port': 15672,
    'proxy': 'proxy_url',
}
config for http
{
    'title': 'title',
    'url': 'url',
    'user': 'user',
    'pass': 'pass',
    'proxy': 'proxy_url',
}
config for telegram
{
    'title': 'title',
    'key': 'key',
    'group': id,
    'proxy': 'proxy_url',
}
config for process 
{
    'pid_file': 'path_to_pid_file'
}
'''
