import os, errno
from checkers.abstract import AbstractChecker
from utils import format_config


class Checker(AbstractChecker):
    ''' '''
    
    _config_required = ('pid_file',)
    _config_allowed = ('pid_file',)
    _config_defaults = {}

    def check(self, config):
        '''
        Main checking function
        every exception from here will be send to notifiers
        @param config: dict
        @return: message or None
        '''
        cfg = format_config(config, self._config_allowed, self._config_required, self._config_defaults)

        pid = None
        try:
            with open(cfg['pid_file'], 'r') as f:
                try:
                    pid = int(f.read())
                except ValueError:
                    return 'Wrong PID value in file'
        except Exception:
            return 'Can\'t read pid_file {}'.format(cfg['pid_file'])

        if pid is 0:
            return 'No process PID'

        try:
            os.kill(pid, 0)
        except OSError as err:
            if err.errno == errno.ESRCH: # no such process
                return 'Process not found'
            elif err.errno == errno.EPERM: # access denied, but there's a process
                return None
            else:
                raise err
        return None
