from checkers.abstract import AbstractChecker
from utils import make_get_request, format_config


class Checker(AbstractChecker):
    ''' '''
    
    _config_required = ('url',)
    _config_allowed = ('title', 'user', 'pass', 'url', 'proxy')
    _config_defaults = {}

    def check(self, config):
        '''
        Main checking function
        every exception from here will be send to notifiers
        @param config: dict
        @return: message or None
        '''
        cfg = format_config(config, self._config_allowed, self._config_required, self._config_defaults)
        r = make_get_request(cfg['url'], cfg)
        r.raise_for_status()
        return None
