import logging
from checkers.abstract import AbstractChecker
from utils import make_get_request, format_config


class Checker(AbstractChecker):
    '''
    Class for checking RabbitMQ memory and disk alerts
    '''

    _api_url = 'api/nodes'
    _config_required = ('user', 'pass', 'host')
    _config_allowed = ('title', 'user', 'pass', 'host', 'port', 'proxy')
    _config_defaults = {'port': 15672}

    def check(self, config):
        '''
        Main checking function
        @param config: dict
        @return: dict
        '''
        cfg = format_config(config, self._config_allowed, self._config_required, self._config_defaults)
        msgs = self._parse_check_data(self._get_check_data(cfg))
        return '\n'.join(msgs) if msgs else None

    def _get_check_data(self, config):
        '''
        Get data from RabbitMQ HTTP API
        @param config: dict
        @return: dict
        '''
        url = "http://{}:{}/{}".format(config['host'], config['port'], self._api_url)
        r = make_get_request(url, config)
        r.raise_for_status()
        return r.json()

    def _parse_check_data(self, data):
        '''
        Parse JSON data from RabbitMQ HTTP API
        @param data: dict
        @return: list
        '''
        if type(data) == type([]):
            tmp = []
            for node in data:
                tmp += self._parse_node_data(node)
            return tmp
        elif type(data) == type({}):
            return self._parse_node_data(data)
        else:
            raise Exception('Can not parse RabbitMQ answer')

    def _parse_node_data(self, data):
        '''
        Parse JSON data of one node from RabbitMQ HTTP API
        @param data: dict
        @return: list
        '''
        if 'mem_alarm' not in data or 'disk_free_alarm' not in data:
            raise Exception('Can not parse RabbitMQ answer')
        logging.debug(data)
        msg = []
        name = data.get('name', 'default')
        if data['mem_alarm']:
            msg.append('Memory alarm on node {}!'.format(name))
        if data['disk_free_alarm']:
            msg.append('Disk alarm on node {}!'.format(name))
        return msg
