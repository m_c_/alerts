class AbstractChecker:
    ''' '''

    _config_required = ()
    _config_allowed = ()
    _config_defaults = {}


    def check(self, config):
        '''
        Main checking function
        every exception from here will be send to notifiers
        @param config: dict
        @return: string message or None
        '''
        raise NotImplemented()
