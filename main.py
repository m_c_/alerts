import importlib
import logging
from config import CONFIG


def main():
    '''
    Main function runs all checkers, collects messages to one and sends to all notifiers
    '''
    if CONFIG['console']:
        logging.basicConfig(
            format='%(asctime)s %(message)s',
            level=logging.DEBUG if CONFIG['debug'] else logging.WARNING
        )
    else:
        logging.basicConfig(
            filename=CONFIG['log'],
            format='%(asctime)s %(message)s',
            level=logging.DEBUG if CONFIG['debug'] else logging.WARNING
        )

    messages = []

    ##### checking #####
    for name in CONFIG['check']:
        try:
            m = importlib.import_module('checkers.{}'.format(name))
            checker = m.Checker()
        except Exception as e:
            err = 'Error creating checker {}: {}'.format(name, e)
            logging.error(err)
            messages.append(err)
            continue

        logging.debug('Сhecker {} imported'.format(name))

        for num, cfg in enumerate(CONFIG['check'][name]):
            try:
                msg = checker.check(cfg)
                if msg is not None:
                    messages.append(msg)
            except Exception as e:
                i = cfg['title'] if 'title' in cfg and cfg['title'] else num
                err = 'Error checking {} {}: {}'.format(name, i, e)
                logging.error(err)
                messages.append(err)
                continue

        logging.debug('Check for {} completed'.format(name))

    ##### notifying #####
    if messages:
        messages = 'Alerts from monitoring:\n' + '\n'.join(messages)
        for name in CONFIG['notify']:
            try:
                m = importlib.import_module('notifiers.{}'.format(name))
                noti = m.Notifier()
            except Exception as e:
                err = 'Error creating notifier {}: {}'.format(name, e)
                logging.error(err)
                messages += '\n' + err
                continue

            logging.debug('Notifier {} created'.format(name))

            for num, cfg in enumerate(CONFIG['notify'][name]):
                cfg['debug'] = CONFIG['debug']
                try:
                    msg = noti.notify(cfg, messages)
                except Exception as e:
                    i = cfg['title'] if 'title' in cfg and cfg['title'] else num
                    err = 'Error checking {} {}: {}'.format(name, i, e)
                    logging.error(err)
                    messages += '\n' + err
                    continue

            logging.debug('Notify for {} completed'.format(name))


if __name__ == '__main__':
    main()
