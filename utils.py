import requests


def make_get_request(url, config=None):
    '''
    @param url: string
    @param config: dict
    @return: Response object from requests
    '''
    auth = None
    proxies = None
    if type(config) == type({}):
        if config['user'] and config['pass']:
            auth = (config['user'], config['pass'])
    
        if config['proxy']:
            proxies = {'http': config['proxy'], 'https': config['proxy']}

    return requests.get(url, auth=auth, proxies=proxies)


def format_config(config, allowed, required, defaults):
    '''
    Check and prepare config
    @param config: dict initial
    @param allowed: iterable
    @param required: iterable
    @param defaults: dict
    @return: dict
    '''
    tmp = {}
    for key in allowed:
        val = None
        if key in config and config[key]:
            val = config[key]
        else:
            if key in required:
                raise Exception('Key "{}" is required in config'.format(key))
            val = defaults[key] if key in defaults else None
        tmp[key] = val
    return tmp
