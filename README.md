Project for sending alerts, consists of checkers and notifiers

Checkers:

1. RabbitMQ memory and disk alerts
2. HTTP request for status 200

Notificators:

1. Telegram

Usage:

1. Clone repository and install requirements from pip.txt
2. Create config.py
3. Run python3 main.py
4. Add checkers and notifiers as needed

config.py minimal example for checking RabbitMQ and sending to telegram

for full config options see base_config.py

```
from base_config import CONFIG
CONFIG['check']['rabbitmq'] = [{
    'user': 'user',
    'pass': 'pass',
    'host': 'host'
}]
CONFIG['notify']['telegram'] = [{
    'key': 'key',
    'group': id
}]
```